<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hoangliet');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

//define('WPLANG', 'vi_VN');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Pf. ZK{>(O!PKiqT$-h%k?lu}rt#BY7fj3N|zamn+2SLv!Y]79w&/3h4/wYJ)Rxs');
define('SECURE_AUTH_KEY',  'V(^0JP[qQEj#y-=64o;d((k=F|q!9h$D40N:fk@^[A<<6Yvus3WX3Yl/p,,x[Y;c');
define('LOGGED_IN_KEY',    'l/zj<EO7sWA7wi:peJ<Ot,Rw<*Znvb&-*:t<:EChC^z=dkH8|s0$Ls$E&V4jSXK9');
define('NONCE_KEY',        'a;C-N{,E2bl9(pF~aGMl>?5`TEd{Pk03T2E<DaG,zH;NQ[k8rF{yJ.NGNp68:H`q');
define('AUTH_SALT',        'PG$w?Th_LN5;P&i6 *ojXG^P/@rj#c#5gY,Q#P]gq>$`cQ~rs;E=)p%I,MP]b(09');
define('SECURE_AUTH_SALT', '@gVd<_QBKs7AOUI.M_dCQTaTH>fyM%(-AJi3N1OmG%y m`sovNyP~c/SyEa`,[Ch');
define('LOGGED_IN_SALT',   '8$Pdx&<U`qB;:>$8e7[7JwaQq`36=sf AdZ8!)0f>ijeDR6HJvMF5S96_5Nbm?9-');
define('NONCE_SALT',       ' |N{y^d5[JT>l<+0-XZXD*YVEgJQ>:B.0j|~`gx^_0W*WQ5rQ8p4p9R)o6#m@8cM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'hl_0817_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
