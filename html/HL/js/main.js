jQuery(document).ready(function($) {
    $('.slider-product-host').owlCarousel({
        margin:15,
        nav:true,
        responsiveClass: true,
        smartSpeed: 1000,
        navText: [
            "<div class='br'><i class='fa fa-angle-left'></i></div>",
            "<div class='br'><i class='fa fa-angle-right'></i></div>"
        ],
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            700:{
                items:4
            }
        }
    });

    $('.product-thumb').owlCarousel({
        margin:10,
        nav:true,
        responsiveClass: true,
        smartSpeed: 1000,
        navText: [
            "<div class='br'><i class='fa fa-angle-left'></i></div>",
            "<div class='br'><i class='fa fa-angle-right'></i></div>"
        ],
        responsive:{
            0:{
                items:2,
            },
            400:{
                items:3,
            },
            1000:{
                items:3
            }
        }
    });

    $('.product-thumb .img-thumb').click(function(event) {
        /* Act on the event */
        event.preventDefault();
        var thumb = $(this).find('img').attr('src');
        $('.wrap-product-detail .image').find('img').attr('src', thumb);
    });

    $('.header-slider').owlCarousel({
        nav:true,
        responsiveClass: true,
        smartSpeed: 1000,
        items:1
    });
    $('.wrap-contact-map .btn').click(function(event) {
        /* Act on the event */
        event.preventDefault();
        $('.wrap-contact-map .btn').removeClass('active');
        $(this).addClass('active');
        var map = $(this).attr('href');
        $('.wrap-map .map').removeClass('active');
        $(map).addClass('active');
    });
    $('.menu-mobile').click(function(event) {
        /* Act on the event */
        event.preventDefault();
        $(this).find('.fa').toggleClass('fa-times').toggleClass('fa-bars');
        $('.main-nav').toggleClass('active');
    });
});
