<div class="col-md-3">
    <div class="list-menu">
        <nav class="menu-product">
            <h4><?php echo __( 'Sản phẩm', 'hoangliet' ) ?></h4>
            <ul>
                <?php
                $terms = get_terms( [
                    'hide_empty'    => false,
                    'taxonomy'      => 'product_categories'
                ] );
                if ( ! empty( $terms ) ) :
                foreach( $terms as $term ) :
                ?>
                <li><a href="<?php echo get_term_link( $term->term_id ) ?>"><?php echo $term->name ?></a></li>
                <?php endforeach; endif; ?>
            </ul>
        </nav>
        <nav class="menu-news">
            <h4><?php echo __( 'Tin tức nổi bật', 'hoangliet' ) ?></h4>
            <?php
            $posts = get_posts( [
                'posts_per_page'   => 4,
                'orderby'          => 'date',
                'order'            => 'DESC',
                'post_type'        => 'post',
                'post_status'      => 'publish',
                'meta_key'         => 'hlmt_hl_is_featured_post',
                'meta_value'       => '1',
            ] );
            if ( ! empty ( $posts ) ) :
            ?>
            <ul>
                <?php foreach( $posts as $post ) : ?>
                <li>
                    <a href="<?php echo get_the_permalink( $post->ID ) ?>" title="<?php echo $post->post_title ?>"><?php echo $post->post_title ?></a>
                </li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
        </nav>
    </div>
</div>