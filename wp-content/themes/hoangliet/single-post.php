<?php get_header() ?>
<!--Begin Main Body-->
<section id="main-body">
    <?php get_template_part( 'template-parts/breadcrumbs' ) ?>
    <!--Begin List Guide-->
    <div class="guide-detail">
        <div class="container">
            <div class="row">
                <?php get_sidebar() ?>
                <div class="col-md-9">
                    <div class="advisory-support">
                        <h2><?php the_title() ?></h2>
                        <div class="content">
                            <?php the_content() ?>
                        </div>
                        <div class="social-posts">
                            <?php echo do_shortcode( '[Sassy_Social_Share title="' . __( 'Chia sẻ bài viết:', 'hoangliet' ) .'"]' ) ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--End List Guide-->
    <?php get_template_part( 'template-parts/featured-products' ) ?>
</section>
<!--End Main Body-->
<?php get_footer() ?>