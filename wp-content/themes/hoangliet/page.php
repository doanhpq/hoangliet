<?php get_header() ?>
<!--Begin Main Body-->
<?php
global $wp;
$url = home_url( add_query_arg( array(), $wp->request ) );
$page_id = url_to_postid( $url );
?>
<section id="main-body">
    <?php get_template_part( 'template-parts/breadcrumbs' ) ?>
    <!--Begin Home Content-->
    <div class="home-content">
        <div class="container">
            <div class="row">
                <?php get_sidebar() ?>
                <div id="main-content" class="col-md-9">
                    <div id="post-<?php echo $page_id ?>" class="advisory-support">
                        <h2 class="title text-left"><?php echo get_the_title( $page_id ) ?></h2>
                        <?php the_content() ?>
                    </div>
                    <div class="social-posts">
                        <?php echo do_shortcode( '[Sassy_Social_Share title="' . __( 'Chia sẻ bài viết:', 'hoangliet' ) .'"]' ) ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--End Home Content-->

    <?php get_template_part( 'template-parts/featured-products' ) ?>

</section>
<!--End Main Body-->
<?php get_footer() ?>