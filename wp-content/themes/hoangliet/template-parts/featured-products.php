<!--Begin Featured Product-->
<div class="home-product">
    <div class="overlay-product"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo __( 'Sản phẩm nổi bật', 'hoangliet' ) ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                $products = get_posts( [
                    'posts_per_page'   => -1,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'meta_key'         => 'hlmt_hl_is_featured_product',
                    'meta_value'       => '1',
                    'post_type'        => 'product',
                    'post_status'      => 'publish'
                ] );
                if ( ! empty ( $products ) ) :
                    ?>
                    <div class="slider-product owl-carousel slider-product-host">
                        <?php foreach( $products as $product ) : ?>
                            <div class="item overclick" data-link="<?php echo get_the_permalink( $product->ID ) ?>">
                                <div class="image">
                                    <a href="<?php echo get_the_permalink( $product->ID ) ?>" title="<?php echo get_the_title( $product->ID ) ?>">
                                        <?php echo get_the_post_thumbnail( $product->ID ) ?>
                                    </a>
                                    <div class="overlay">
                                        <div class="product-code">
                                            <p><?php echo __( 'Mã số sản phẩm:', 'hoangliet' ) ?><br /> <?php echo get_post_meta( $product->ID, 'hlmt_hl_product_code', true ) ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-order">
                                    <a href="<?php echo hl_get_order_page_link( $product->ID ); ?>" class="btn"><?php echo __( 'Đặt hàng', 'hoangliet' ) ?></a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<!--End Featured Product-->