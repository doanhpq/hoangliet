<?php
/* Template Name: đặt hàng */
get_header();
global $option_tree;
?>
<style type="text/css">
    .heateor_sss_sharing_container {display: none !important}
</style>
<?php
$captcha_instance = new ReallySimpleCaptcha();
$captcha_instance->img_size = [112, 33];
$captcha_instance->font_char_width = 25;
$word = $captcha_instance->generate_random_word();
$prefix = mt_rand();
$captcha_image_name = $captcha_instance->generate_image( $prefix, $word );
$captcha_image_file_path = plugins_url().'/really-simple-captcha/tmp/'.$captcha_image_name;
$error = '';

$pid = trim( strip_tags( $_GET['pid'] ) );
$product = get_post( $pid );
if ( isset( $_POST['submit'] ) ) {
    $post_fields = [
        'fullname',
        'email',
        'address',
        'company_name',
        'phone',
        'content',
        'product_name',
        'quantity',
        'captcha',
        'prefix_name'
    ];

    foreach( $post_fields as $field ) {
        $field_value = trim( strip_tags( $_POST[$field] ) );
        if ( empty ( $field_value ) ) {
            $error = ['message' => __( 'Tất cả các trường không được để trống', 'hoangliet' ), 'status' => 'error'];
            break;
        } else {
            $$field = $field_value;
        }
    }

    if ( empty ( $error ) ) {
        if ( $captcha_instance->check( $prefix_name, $captcha ) ) {
            $to = ['phongtranh68@gmail.com'];
            $to_setting = $option_tree['hl_opt_receive_email'];
            if( ! empty( $to_setting ) )
                $to[] = $option_tree['hl_opt_receive_email'];

$message = "
Chào bạn!<br />
Bạn có đơn hàng mới từ website " . home_url() ."<br /><br />
Họ tên: " . $fullname . "<br /><br />
Email: " . $email . "<br /><br />
Địa chỉ: " . $address . "<br /><br />
Tên công ty: " . $company_name . "<br /><br />
Số điện thoại: " . $phone . "<br /><br />
Lời nhắn: " . $content . "<br /><br />
Tên sản phẩm: <b>" . $product_name . "</b><br /><br />
Số lượng: <b>" . $quantity . "</b><br /><br />
";
            wp_mail( $to, '[Hoàng Liệt] Đơn hàng mới', $message );
            $captcha_instance->remove( $prefix_name );

            global $wp;
            $redirect_link = home_url(add_query_arg(array(),$wp->request) . '?order=success');
            echo '<script type="text/javascript">window.location.href="' . $redirect_link . '"</script>';
            exit;
        }
    }
}
?>
<!--Begin Main Body-->
<section id="main-body">
    <?php get_template_part( 'template-parts/breadcrumbs' ) ?>
    <!--Begin Contact Content-->
    <div class="wrap-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title"><?php the_title() ?></h2>
                    <div class="info-company">
                        <?php the_content() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Contact Content-->

    <!--Begin Contact Form-->
    <div class="wrap-contact-form">
        <form action="" method="post">
            <div class="container">
                <div class="row" style="margin-left: 1px;">
                    <?php
                    if ( ! empty ( $error ) && $error['status'] === 'error' ) {
                        echo '<p class="error">' . $error['message'] . '</p>';
                    } elseif( isset( $_GET['order'] ) && $_GET['order'] === 'success' ) {
                        echo '<p class="success">' . __( 'Bạn đã đặt hàng thành công. Chúng tôi sẽ liên hệ với bạn vào thời gian sớm nhất.' ) . '</p>';
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-line">
                            <input value="<?php echo isset( $fullname ) ? $fullname : '' ?>" name="fullname" type="text" placeholder="* Họ và tên:" required />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-line">
                            <input value="<?php echo isset( $email ) ? $email : '' ?>" name="email" type="email" placeholder="* Email:" required />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-line">
                            <input value="<?php echo isset( $address ) ? $address : '' ?>" name="address" type="text" placeholder="* Địa chỉ:" required />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-line">
                            <input value="<?php echo isset( $company_name ) ? $company_name : '' ?>" name="company_name" type="text" placeholder="* Tên công ty:" required />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-line">
                            <input value="<?php echo isset( $phone ) ? $phone : '' ?>" name="phone" type="text" placeholder="* Số điện thoại:" required />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="textarea-content">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-line">
                                <textarea name="content" placeholder="* Nội dung" required><?php echo isset( $content ) ? $content : '' ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-line">
                                        <input type="text" name="captcha" placeholder="* Mã số:" required />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-line">
                                        <?php echo '<img id="captcha" src="'.$captcha_image_file_path.'">'; ?>
                                        <input type="hidden" name="prefix_name" value="<?php echo $prefix ?>" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-line">
                            <input value="<?php
                                if ( isset( $product_name ) )
                                    echo $product_name;
                                elseif( isset( $product ) && ! empty( $product ) )
                                    echo $product->post_title;
                            ?>" name="product_name" type="text" placeholder="* Tên của sản phẩm" required />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-line">
                            <input value="<?php echo isset( $quantity ) ? $quantity : '' ?>" name="quantity" type="number" placeholder="* Số lượng:" required />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-line">
                            <button type="submit" name="submit">GỬI ĐI</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!--End Contact Form-->

    <!--Begin Contact Map-->
    <div class="wrap-contact-map">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="#office-hn" class="btn bd-gray office-hn active">VĂN PHÒNG HÀ NỘI</a></li>
                        <li><a href="#office-qn" class="btn bd-gray">VĂN PHÒNG QUẢNG NINH</a></li>
                    </ul>
                    <div class="wrap-map">
                        <div id="office-hn" class="map active">
                            <?php echo get_post_meta( get_the_ID(), 'hlmt_hl_hanoi_address', true ) ?>
                        </div>
                        <div id="office-qn" class="map">
                            <?php echo get_post_meta( get_the_ID(), 'hlmt_hl_quangninh_address', true ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--End Contact Map-->
    <?php get_template_part( 'template-parts/featured-products' ) ?>
</section>
<!--End Main Body-->
<?php get_footer() ?>
