<?php get_header() ?>
    <!--Begin Main Body-->
    <section id="main-body">
        <?php get_template_part( 'template-parts/breadcrumbs' ) ?>
        <!--Begin List Guide-->
        <div class="guide-detail">
            <div class="container">
                <div class="row">
                    <?php get_sidebar() ?>
                    <div class="col-md-9">
                        <div class="list-products home-product">
                            <h2><?php single_term_title(); ?></h2>
                            <div class="slider-product">
                                <div class="row">
                                    <?php
                                    $taxonomy_id = get_queried_object()->term_id;
                                    $page = get_query_var( 'paged' );
                                    $post_per_page = get_option('posts_per_page');
                                    if( $page >= 1 ) $page -= 1;
                                    $offset = $page * ( int ) $post_per_page;
                                    $products = get_posts( [
                                        'posts_per_page'   => ( int ) $post_per_page,
                                        'offset'        =>  $offset,
                                        'orderby'          => 'date',
                                        'order'            => 'DESC',
                                        'post_type'        => 'product',
                                        'post_status'      => 'publish',
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'product_categories',
                                                'field' => 'term_id',
                                                'terms' => [$taxonomy_id],
                                            )
                                        )
                                    ] );

                                    if ( ! empty ( $products ) ) :
                                        foreach( $products as $product ) :
                                            ?>
                                            <div class="col-md-4">
                                                <div class="item overclick" data-link="<?php echo get_the_permalink( $product->ID ) ?>">
                                                    <div class="image">
                                                        <a href="<?php echo get_the_permalink( $product->ID ) ?>" title="<?php echo get_the_title( $product->ID ) ?>">
                                                            <?php echo get_the_post_thumbnail( $product->ID ) ?>
                                                        </a>
                                                        <div class="overlay">
                                                            <div class="product-code">
                                                                <p><?php echo __( 'Mã số sản phẩm:', 'hoangliet' ) ?><br /> <?php echo get_post_meta( $product->ID, 'hlmt_hl_product_code', true ) ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-order">
                                                        <a href="<?php echo hl_get_order_page_link( $product->ID ); ?>" class="btn bd-gray"><?php echo __( 'Đặt hàng', 'hoangliet' ) ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; endif; ?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div>
                                <div class="row">
                                    <div class="col-md-6 social-posts">
                                        <?php echo do_shortcode( '[Sassy_Social_Share title="' . __( 'Chia sẻ bài viết:', 'hoangliet' ) .'"]' ) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="pagination" class="pull-right">
                                            <?php
                                            the_posts_pagination( array(
                                                'mid_size' => 3,
                                                'prev_text' => '<',
                                                'next_text' => '>',
                                                'screen_reader_text'    =>  '&nbsp;'
                                            ) );
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!--End List Guide-->
    </section>
    <!--End Main Body-->
<?php get_footer() ?>