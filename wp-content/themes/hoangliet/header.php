<?php
global $option_tree;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta content="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php endif; ?>
    <title><?php wp_title( ' - ', true, 'right' ); ?></title>
    <link rel="icon" href="<?php echo $option_tree['hl_opt_favicon'] ?>" type="image/x-icon" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <?php wp_head() ?>
    <style type="text/css">
        a:hover {
            text-decoration: none;
        }
        .breadcrumbs {
            background: transparent;
        }
        #main-content img {
            max-width: 100%;
        }
        #main-content h5 {
            font-size: 15px;
            font-weight: bold;
            color: #0081c7;
            margin-bottom: 15px;
            text-transform: uppercase;
        }
        .list-guide:not(.news-hot) .item .image img {
            max-width: 131px;
            height: auto;
        }
        .pagination .page-numbers {
            padding: 5px 10px;
            border: 1px solid #c6c6c6;
            font-weight: normal;
            transition: 1s;
        }
        .pagination .page-numbers.current {
            background: #0081c7;
            border-color: #0081c7;
            color: #fff;
        }
        .pagination .page-numbers:hover {
            text-decoration: none;
            background: #0081c7;
            color: #ffffff;

        }
        .guide-detail .content img {
            max-width: 100%;
            width: auto;
        }
        .wrap-product-detail .product-thumb .item img {
            width: 90px;
            height: 76px;
        }
        textarea {
            padding: 10px;
        }
        .error, .success {
            color: red;
            border: 1px solid;
            padding: 10px;
            font-weight: bold;
        }
        .success {
            color: green;
        }
        h2.title, .advisory-support > h2 {
            text-transform: uppercase;
        }
        .list-guide .item .content p, .list-product-us .content {
            line-height: 21px;
        }
        .list-guide.news-hot .item .content {
            padding-top: 10px;
        }
        .list-guide .item .content a:hover h6, .list-guide .item .content a:active h6, .list-guide .item .content a:hover {
            color: #0081c7 !important;
            text-decoration: none;
        }
        .list-product-us .item .content {
            padding: 15px 25px;
            width: 50%;
            float: left;
        }
        .list-product-us .item .content h4, .main-nav > ul > li > a {
            text-transform: uppercase;
        }
        .list-product-us .item .content {
            line-height: 22px;
        }
		.main-nav>ul>li ul {
			width: 205px;
		}
    </style>
</head>
<body <?php body_class(); ?>>
<!--Begin Wrapper-->
<div id="wrapper">
	<a href="tel:+84905711888" class="phone-header">
		<img src="<?php echo IMAGE_URL ?>icon-phone.png" alt="icon phone">
	</a>
    <!--Begin Header-->
    <header class="header-home header-default">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="logo">
                        <a href="<?php echo home_url(); ?>">
                            <img src="<?php echo $option_tree['hl_opt_logo'] ?>" alt="logo">
                        </a>
                    </div>
                    <div class="wrap-menu-header">
                        <div class="menu-top">
                            <div class="item hotline">
								<i class="fa fa-phone"></i> <span>Hotline: <?php echo $option_tree['hl_opt_hotline'] ?></span>
                            </div>
                            <div class="item search-header">
                                <form role="search" action="<?php echo hl_get_search_link(); ?>" method="get" id="searchform">
                                    <input type="text" value="<?php echo get_search_query() ?>" name="s" placeholder="Tìm kiếm...">
                                    <input type="hidden" name="post_type" value="product" />
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <div class="item languages">
                                <a href="<?php echo home_url( 'vi' ); ?>" class="<?php echo url_contains( '/vi/' ) ? 'active' : '' ?>">
                                    <img src="<?php echo IMAGE_URL ?>vn.png" alt="lng-vn">
                                </a>
                                <a href="<?php echo home_url( 'en' ); ?>" class="<?php echo url_contains( '/en/' ) ? 'active' : '' ?>">
                                    <img src="<?php echo IMAGE_URL ?>en.png" alt="lng-en">
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <nav class="main-nav">
                            <?php wp_nav_menu( [
                                'theme_location'  => 'primary',
                                'container' => ''
                            ] ); ?>
                        </nav>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </header>
    <!--End Header-->
    <?php
    if ( is_singular() ) {
        global $wp;
        $url = home_url( add_query_arg( array(), $wp->request ) );
        $post_id = url_to_postid( $url );
        $slider_shortcode = get_post_meta( $post_id, 'hlmt_hl_slider', true );
    } elseif ( is_tax( 'product_categories' ) ) {
        $taxonomy_id = get_queried_object()->term_id;
        $slider_shortcode = hl_get_product_categories_meta( 'hl_cat_slider', $taxonomy_id );
    } elseif ( is_category() ) {
        $taxonomy_id = get_queried_object()->term_id;
        $slider_shortcode = get_option( 'category_' . $taxonomy_id . '_hl_cat_slider' );
    } else {
        $slider_shortcode = '[rev_slider alias="slider-trang-chu"]';
    }
    if( empty ( $slider_shortcode ) )
        $slider_shortcode = '[rev_slider alias="slider-trang-chu"]';
    ?>
    <?php echo do_shortcode( $slider_shortcode ) ?>
	
    <!--Begin Function-->
    <div class="wrap-function">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="function">
                        <div class="image">
                            <img src="<?php echo IMAGE_URL ?>icon-headphone.png" alt="icon headphone">
                        </div>
                        <div class="info">
                            <?php if ( is_active_sidebar( 'hl_support_info_1' ) ) dynamic_sidebar( 'hl_support_info_1' ) ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="function">
                        <div class="image">
                            <img src="<?php echo IMAGE_URL ?>icon-setting.png" alt="icon setting">
                        </div>
                        <div class="info">
                            <?php if ( is_active_sidebar( 'hl_support_info_1' ) ) dynamic_sidebar( 'hl_support_info_1' ) ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="function">
                        <div class="image">
                            <img src="<?php echo IMAGE_URL ?>icon-cal.png" alt="icon cal">
                        </div>
                        <div class="info">
                            <?php if ( is_active_sidebar( 'hl_support_info_1' ) ) dynamic_sidebar( 'hl_support_info_1' ) ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--End Function-->