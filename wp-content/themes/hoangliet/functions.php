<?php
require 'inc/constants.php';
require 'inc/register_product_post_type.php';
require 'inc/register_product_taxonomy.php';
require 'inc/meta_box.php';
require 'inc/helpers/helpers.php';

load_theme_textdomain( 'hoangliet', get_template_directory() . '/languages' );

add_action( 'after_setup_theme', 'hl_setup_theme_support' );

function hl_setup_theme_support() {
    add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'automatic-feed-link' );
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
    add_theme_support( 'title-tag' );
    register_nav_menus( array(
        'primary' => 'Menu chính',
        'footer_menu' => 'Menu chân trang'
    ) );
}

add_action( 'widgets_init', 'hl_widgets_init' );

function hl_widgets_init() {
    register_sidebar( array(
        'name'          => 'Chân trang 1',
        'id'            => 'hl_footer_1',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<p style="display: none">',
        'after_title'   => '</p>',
    ) );

    register_sidebar( array(
        'name'          => 'Chân trang 2',
        'id'            => 'hl_footer_2',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<p style="display: none">',
        'after_title'   => '</p>',
    ) );

    register_sidebar( array(
        'name'          => 'Chân trang 3',
        'id'            => 'hl_footer_3',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<p style="display: none">',
        'after_title'   => '</p>',
    ) );

    register_sidebar( array(
        'name'          => 'Chân trang 4',
        'id'            => 'hl_footer_4',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<p style="display: none">',
        'after_title'   => '</p>',
    ) );

    register_sidebar( array(
        'name'          => 'Thông tin hỗ trợ 1',
        'id'            => 'hl_support_info_1',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<p style="display: none">',
        'after_title'   => '</p>',
    ) );

    register_sidebar( array(
        'name'          => 'Thông tin hỗ trợ 2',
        'id'            => 'hl_support_info_2',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<p style="display: none">',
        'after_title'   => '</p>',
    ) );

    register_sidebar( array(
        'name'          => 'Thông tin hỗ trợ 3',
        'id'            => 'hl_support_info_3',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<p style="display: none">',
        'after_title'   => '</p>',
    ) );

    register_sidebar( array(
        'name'          => 'Box giới thiệu',
        'id'            => 'hl_intro_box',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<p style="display: none">',
        'after_title'   => '</p>',
    ) );
}

add_action( 'wp_enqueue_scripts', 'hl_enqueue_scripts' );

function hl_enqueue_scripts() {
    if ( ! is_admin() ) {
        wp_enqueue_style( 'font-awesome', STYLE_URL . 'font-awesome/css/font-awesome.min.css', false, '1.0' );
        wp_enqueue_style( 'bootstrap', STYLE_URL . 'bootstrap/bootstrap.min.css', false, '1.0' );
        wp_enqueue_style( 'main', STYLE_URL . 'main.css', false, '1.0' );

        wp_enqueue_script( 'jquery', SCRIPT_URL . '/plugin/jquery.min.js', false, '3.2', true );
        wp_enqueue_script( 'tether', SCRIPT_URL . '/plugin/tether.min.js', false, '1.0', true );
        wp_enqueue_script( 'bootstrap', SCRIPT_URL . '/plugin/bootstrap.min.js', false, '1.0', true );
        wp_enqueue_script( 'carousel', SCRIPT_URL . '/plugin/owl.carousel.min.js', false, '1.0', true );
        wp_enqueue_script( 'main', SCRIPT_URL . '/main.js', false, '3.2', true );
    }
}

function wpse27856_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','wpse27856_set_content_type' );

function hl_template_chooser( $template )
{
    global $wp_query;
    $post_type = get_query_var('post_type');
    if( $wp_query->is_search && $post_type == 'product' )
    {
        return locate_template('archive-search.php');
    }
    return $template;
}
add_filter('template_include', 'hl_template_chooser');

$option_tree = get_option( 'option_tree' );

