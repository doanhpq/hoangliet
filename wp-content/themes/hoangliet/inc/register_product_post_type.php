<?php
class HL_Product {

    public function __construct()
    {
        add_action( 'init', [$this, 'add_post_type'] );
    }

    public function add_post_type() {
        $labels = array(
            'name'                  => __( 'Sản Phẩm', 'hoangliet' ),
            'singular_name'         => __( 'Sản Phẩm', 'hoangliet' ),
            'menu_name'             => __( 'Sản Phẩm', 'hoangliet' ),
            'name_admin_bar'        => __( 'Sản Phẩm', 'hoangliet' ),
            'archives'              => __( 'Danh sách sản phẩm', 'hoangliet' ),
            'attributes'            => __( 'Thuộc tính', 'hoangliet' ),
            'parent_item_colon'     => __( 'Sản phẩm cha', 'hoangliet' ),
            'all_items'             => __( 'Tất cả sản phẩm', 'hoangliet' ),
            'add_new_item'          => __( 'Thêm sản phẩm mới', 'hoangliet' ),
            'add_new'               => __( 'Thêm mới', 'hoangliet' ),
            'new_item'              => __( 'Sản phẩm mới', 'hoangliet' ),
            'edit_item'             => __( 'Sửa', 'hoangliet' ),
            'update_item'           => __( 'Cập nhật', 'hoangliet' ),
            'view_item'             => __( 'Xem', 'hoangliet' ),
            'view_items'            => __( 'Xem', 'hoangliet' ),
            'search_items'          => __( 'tìm kiếm', 'hoangliet' ),
            'not_found'             => __( 'Không tìm thấy', 'hoangliet' ),
            'not_found_in_trash'    => __( 'Không tìm thấy trong thùng rác', 'hoangliet' ),
            'featured_image'        => __( 'Ảnh đại diện', 'hoangliet' ),
            'set_featured_image'    => __( 'Chọn ảnh đại diện', 'hoangliet' ),
            'remove_featured_image' => __( 'Xóa ảnh đại diện', 'hoangliet' ),
            'use_featured_image'    => __( 'Dùng làm ảnh đại diện', 'hoangliet' ),
            'insert_into_item'      => __( 'Chèn vào sản phẩm', 'hoangliet' ),
            'uploaded_to_this_item' => __( 'Cập nhật sản phẩm', 'hoangliet' ),
            'items_list'            => __( 'Danh sách', 'hoangliet' ),
            'items_list_navigation' => __( 'Items list navigation', 'hoangliet' ),
            'filter_items_list'     => __( 'Lọc', 'hoangliet' ),
        );
        $rewrite = array(
            'slug'                  => __( 'san-pham', 'hoangliet' ),
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => false,
        );
        $args = array(
            'label'                 => __( 'Sản Phẩm', 'hoangliet' ),
            'description'           => __( 'Custom product post type', 'hoangliet' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'custom-fields', ),
            'taxonomies'            => array( 'product_categories' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-tickets-alt',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'post',
        );
        register_post_type( 'product', $args );
    }
}

new HL_Product;