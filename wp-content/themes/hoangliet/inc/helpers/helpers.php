<?php
function p( $param ) {
    echo '<pre>';
    print_r( $param );
    echo '</pre>';
    die;
}

function v( $param ) {
    echo '<pre>';
    var_dump( $param );
    echo '</pre>';
    die;
}

function hl_get_product_categories_meta( $key, $id ) {
    return get_option( 'product_categories_' . $id . '_' . $key );
}

function hl_get_order_page_link( $product_id = '' ) {
    if ( url_contains( '/en/' ) )
        return get_page_link( 127 ) . '?pid=' . $product_id;

    return get_page_link( 125 ) . '?pid=' . $product_id;
}

function hl_get_search_link() {
    if ( url_contains( '/en/' ) )
        return home_url( 'en/' );

    return home_url( 'vi/' );
}

function url_contains( $needles )
{
    return str_contains( $_SERVER['REQUEST_URI'], $needles, false );
}

function str_contains( $haystack, $needles, $case_sensitive = true )
{
    foreach ( (array) $needles as $needle )
    {
        if ( ! $case_sensitive )
        {
            $haystack 	= strtolower( $haystack );
            $needle		= strtolower( $needle );
        }

        if ( $needle != '' && strpos( $haystack, $needle ) !== false )
            return true;
    }

    return false;
}

function hl_limit_excerpt( $content, $limit ) {
    if( strlen( $content ) > $limit ) {
        $content = substr( $content, 0, $limit );
        return substr( $content, 0, strrpos( $content, ' ' ) );
    }
    return $content;
}