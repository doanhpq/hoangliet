<?php
class HL_Taxonomies {

    public function __construct()
    {
        add_action( 'init', [$this, 'add_taxonomy'] );
    }

    public function add_taxonomy() {

        $labels = array(
            'name'                       => _x( 'Danh mục sản phẩm', 'Taxonomy General Name', 'hoangliet' ),
            'singular_name'              => _x( 'Danh mục sản phẩm', 'Taxonomy Singular Name', 'hoangliet' ),
            'menu_name'                  => __( 'Danh mục sản phẩm', 'hoangliet' ),
            'all_items'                  => __( 'Tất cả', 'hoangliet' ),
            'parent_item'                => __( 'Danh mục cha', 'hoangliet' ),
            'parent_item_colon'          => __( 'Danh mục cha:', 'hoangliet' ),
            'new_item_name'              => __( 'Danh mục mới', 'hoangliet' ),
            'add_new_item'               => __( 'Thêm mới', 'hoangliet' ),
            'edit_item'                  => __( 'Sửa', 'hoangliet' ),
            'update_item'                => __( 'Cập nhật', 'hoangliet' ),
            'view_item'                  => __( 'Xem', 'hoangliet' ),
            'separate_items_with_commas' => __( 'Phân cách bằng dấu phẩy', 'hoangliet' ),
            'add_or_remove_items'        => __( 'Thêm hoặc xóa danh mục', 'hoangliet' ),
            'choose_from_most_used'      => __( 'Chọn từ danh mục được dùng nhiều nhất', 'hoangliet' ),
            'popular_items'              => __( 'Danh mục thông dụng', 'hoangliet' ),
            'search_items'               => __( 'Tìm kiếm', 'hoangliet' ),
            'not_found'                  => __( 'Không tìm thấy', 'hoangliet' ),
            'no_terms'                   => __( 'Không có danh mục nào', 'hoangliet' ),
            'items_list'                 => __( 'Danh sách', 'hoangliet' ),
            'items_list_navigation'      => __( 'Items list navigation', 'hoangliet' ),
        );
        $rewrite = array(
            'slug'                       => __('danh-muc', 'hoangliet'),
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
            'show_in_menu'               => true,
            'query_var'         => true,
        );
        register_taxonomy( 'product_categories', array( 'product' ), $args );
    }

}

new HL_Taxonomies;