<?php
define( 'THEME_URL', get_stylesheet_directory_uri() );
define( 'STYLE_URL', THEME_URL . '/css/' );
define( 'SCRIPT_URL', THEME_URL . '/js/' );
define( 'IMAGE_URL', THEME_URL . '/images/' );