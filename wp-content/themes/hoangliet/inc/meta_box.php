<?php
class HL_Metabox {

    public function __construct()
    {
        add_filter( 'rwmb_meta_boxes', [$this, 'add_meta_box_to_product'] );
        add_filter( 'rwmb_meta_boxes', [$this, 'add_page_meta'] );
        add_filter( 'rwmb_meta_boxes', [$this, 'add_post_meta'] );
    }

    public function add_meta_box_to_product( $meta_boxes ) {
        $prefix = 'hlmt_';

        $meta_boxes[] = array(
            'id' => 'hl_advance_option',
            'title' => esc_html__( 'Tùy chọn nâng cao', 'hoangliet' ),
            'post_types' => array( 'product' ),
            'context' => 'advanced',
            'priority' => 'default',
            'autosave' => false,
            'fields' => array(
                array(
                    'id' => $prefix . 'hl_product_code',
                    'type' => 'text',
                    'name' => esc_html__( 'Mã sản phẩm', 'hoangliet' ),
                ),
                array(
                    'id' => $prefix . 'hl_is_featured_product',
                    'name' => esc_html__( 'Là sản phẩm nổi bật', 'hoangliet' ),
                    'type' => 'checkbox',
                ),
                array(
                    'id' => $prefix . 'hl_slider',
                    'type' => 'text',
                    'name' => esc_html__( 'Slider shortcode', 'hoangliet' ),
                ),
                array(
                    'id' => $prefix . 'hl_product_images',
                    'type' => 'image_advanced',
                    'name' => esc_html__( 'Hình ảnh sản phẩm', 'hoangliet' ),
                    'desc' => esc_html__( 'Chọn 1 hoặc nhiều hình ảnh', 'hoangliet' ),
                    'force_delete' => true,
                    'max_file_uploads' => '20',
                ),
            ),
        );

        return $meta_boxes;
    }

    function add_page_meta( $meta_boxes ) {
        $prefix = 'hlmt_';

        $meta_boxes[] = array(
            'id' => 'hl_slider',
            'title' => esc_html__( 'Tùy chọn nâng cao', 'hoangliet' ),
            'post_types' => array( 'page' ),
            'context' => 'advanced',
            'priority' => 'default',
            'autosave' => false,
            'fields' => array(
                array(
                    'id' => $prefix . 'hl_slider',
                    'type' => 'text',
                    'name' => esc_html__( 'Nhập slider shortcode', 'hoangliet' ),
                ),
                array(
                    'id' => $prefix . 'hl_hanoi_address',
                    'type' => 'textarea',
                    'name' => esc_html__( 'Địa chỉ Hà Nội', 'hoangliet' ),
                    'rows' => 4,
                ),
                array(
                    'id' => $prefix . 'hl_quangninh_address',
                    'type' => 'textarea',
                    'name' => esc_html__( 'Địa chỉ Quảng Ninh', 'hoangliet' ),
                    'rows' => 4,
                ),
            ),
        );

        return $meta_boxes;
    }

    function add_post_meta( $meta_boxes ) {
        $prefix = 'hlmt_';

        $meta_boxes[] = array(
            'id' => 'hl_slider',
            'title' => esc_html__( 'Tùy chọn nâng cao', 'hoangliet' ),
            'post_types' => array( 'post' ),
            'context' => 'advanced',
            'priority' => 'default',
            'autosave' => false,
            'fields' => array(
                array(
                    'id' => $prefix . 'hl_slider',
                    'type' => 'text',
                    'name' => esc_html__( 'Nhập slider shortcode', 'hoangliet' ),
                ),
                array(
                    'id' => $prefix . 'hl_is_featured_post',
                    'name' => esc_html__( 'Là tin nổi bật', 'hoangliet' ),
                    'type' => 'checkbox',
                ),
				array(
                    'id' => $prefix . 'hl_show_in_homepage',
                    'name' => esc_html__( 'Hiện ngoài trang chủ', 'hoangliet' ),
                    'type' => 'checkbox',
                ),
            ),
        );

        return $meta_boxes;
    }

}

new HL_Metabox;