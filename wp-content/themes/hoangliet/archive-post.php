<?php get_header() ?>
<!--Begin Main Body-->
<section id="main-body">
    <?php get_template_part( 'template-parts/breadcrumbs' ) ?>
    <!--Begin List Guide-->
    <div class="wrap-list-guide">
        <div class="container">
            <div class="row">
                <?php get_sidebar() ?>
                <div class="col-md-9">
                    <div class="advisory-support">
                        <h2><?php echo single_cat_title( '', false ) ?></h2>
                        <div class="list-guide">
                            <div class="row">
                                <?php
								$taxonomy_id = get_queried_object()->term_id;
								$page = get_query_var( 'paged' );
                                $post_per_page = get_option('posts_per_page');
                                if( $page >= 1 ) $page -= 1;
                                $offset = $page * ( int ) $post_per_page;
                                $posts = get_posts( [
                                    'posts_per_page'   => ( int ) $post_per_page,
                                    'offset'        =>  $offset,
                                    'orderby'          => 'date',
                                    'order'            => 'DESC',
                                    'post_type'        => 'post',
                                    'post_status'      => 'publish',
									'tax_query' => array(
										array(
											'taxonomy' => 'category',
											'field' => 'term_id',
											'terms' => [$taxonomy_id],
										)
									)
                                ] );
                                if ( ! empty ( $posts ) ) :
                                    foreach( $posts as $post ) :
                                ?>
                                <div class="col-md-6">
                                    <div class="item">
                                        <div class="image">
                                            <?php echo get_the_post_thumbnail( $post->ID ) ?>
                                        </div>
                                        <div class="content">
                                            <h6><a href="<?php echo get_the_permalink( $post->ID ) ?>" title="<?php echo get_the_title( $post->ID ) ?>"><?php echo get_the_title( $post->ID ) ?></a></h6>
                                            <p class="date-comment"><i class="fa fa-calendar"></i> <?php echo get_the_date( 'd/m/Y', $post->ID ) ?> <span><?php  ?> <?php $comment_count = get_comment_count( $post->ID ); echo $comment_count['approved']; ?> <?php echo __( 'bình luận', 'hoangliet' ) ?></span></p>
                                            <p><?php echo hl_limit_excerpt( get_the_excerpt( $post->ID ), 100 ) ?></p>
                                            <a href="<?php echo get_the_permalink( $post->ID ) ?>" class="btn bd-blue" title="<?php echo __( 'Xem thêm', 'hoangliet' ) ?>"><?php echo __( 'Xem thêm', 'hoangliet' ) ?></a>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; endif; wp_reset_postdata() ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-md-6 social-posts">
                                    <?php echo do_shortcode( '[Sassy_Social_Share title="' . __( 'Chia sẻ bài viết:', 'hoangliet' ) .'"]' ) ?>
                                </div>
                                <div class="col-md-6">
                                    <div id="pagination" class="pull-right">
                                        <?php
                                        the_posts_pagination( array(
                                            'mid_size' => 3,
                                            'prev_text' => '<',
                                            'next_text' => '>',
                                            'screen_reader_text'    =>  '&nbsp;'
                                        ) );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--End List Guide-->
    <?php get_template_part( 'template-parts/featured-products' ) ?>
</section>
<!--End Main Body-->
<?php get_footer() ?>