<?php get_header() ?>
<!--Begin Main Body-->
<section id="main-body">
    <?php get_template_part( 'template-parts/breadcrumbs' ) ?>
    <!--Begin Home Content-->
    <div class="home-content">
        <div class="container">
            <div class="row">
                <?php get_sidebar() ?>
                <?php
                $product = get_query_var( 'product' );
                $products = get_posts( [
                    'name'  => $product,
                    'post_type' => 'product',
                    'posts_per_page'    => 1,
                    'post_status'   => 'publish'
                ] );
                if ( isset( $products[0] ) && ! empty ( $products[0] ) ) :
                    $product = $products[0];
                ?>
                <div class="col-md-9">
                    <div class="wrap-product-detail">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="image">
                                    <img src="<?php echo get_the_post_thumbnail_url( $product->ID, 'full' ) ?>" alt="product detail">
                                </div>
                                <?php
                                $list_images = get_post_meta( $product->ID, 'hlmt_hl_product_images' );
                                if ( ! empty ( $list_images ) ) :
                                ?>
                                <div class="product-thumb owl-carousel">
                                    <?php foreach( $list_images as $image ) : ?>
                                    <div class="item">
                                        <a href="javascript:;" class="img-thumb">
                                            <img src="<?php $img = wp_get_attachment_image_src( $image, 'full' ); echo $img[0] ?>" alt="product-thumb">
                                        </a>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-7">
                                <div class="content-detail">
                                    <h3><?php echo __( 'Mã số sản phẩm:', 'hoangliet' ) ?> <?php echo get_post_meta( $product->ID, 'hlmt_hl_product_code', true ) ?></h3>
                                    <p><strong><?php echo __( 'Chi tiết sản phẩm:', 'hoangliet' ) ?></strong></p>
                                    <p><?php echo $product->post_content ?></p>
                                    <a href="<?php echo hl_get_order_page_link( $product->ID ); ?>" class="btn bd-blue"><?php echo __( 'Đặt hàng', 'hoangliet' ) ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="social-posts">
                        <?php echo do_shortcode( '[Sassy_Social_Share title="' . __( 'Chia sẻ bài viết:', 'hoangliet' ) .'"]' ) ?>
                    </div>
                </div>
                <?php endif; ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--End Home Content-->

    <!--Begin Home Product-->
    <div class="home-product">
        <div class="overlay-product"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><?php echo __( 'Sản phẩm liên quan', 'hoangliet' ) ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $categories = get_the_terms( $id, 'product_categories' );
                    $categories = wp_list_pluck( $categories, 'term_id' );
                    $products = get_posts( [
                        'posts_per_page'   => 10,
                        'orderby'          => 'date',
                        'order'            => 'DESC',
                        'post_type'        => 'product',
                        'post_status'      => 'publish',
                        'exclude'          => [$product->ID],
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'product_categories',
                                'field' => 'term_id',
                                'terms' => $categories,
                            )
                        )
                    ] );

                    if ( ! empty ( $products ) ) :
                        ?>
                        <div class="slider-product owl-carousel slider-product-host">
                            <?php foreach( $products as $product ) : ?>
                                <div class="item overclick" data-link="<?php echo get_the_permalink( $product->ID ) ?>">
                                    <div class="image">
                                        <a href="<?php echo get_the_permalink( $product->ID ) ?>" title="<?php echo get_the_title( $product->ID ) ?>">
                                            <?php echo get_the_post_thumbnail( $product->ID ) ?>
                                        </a>
                                        <div class="overlay">
                                            <div class="product-code">
                                                <p><?php echo __( 'Mã số sản phẩm:', 'hoangliet' ) ?><br /> <?php echo get_post_meta( $product->ID, 'hlmt_hl_product_code', true ) ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-order">
                                        <a href="<?php echo hl_get_order_page_link( $product->ID ); ?>" class="btn"><?php echo __( 'Đặt hàng', 'hoangliet' ) ?></a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <!--End Home Product-->

</section>
<!--End Main Body-->
<?php get_footer() ?>