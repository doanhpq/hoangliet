<?php
global $option_tree;
get_header();
?>
<!--Begin Main Body-->
<section id="main-body">
    <!--Begin Home Product-->
    <div class="home-product-us">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10 text-center">
                    <?php if ( is_active_sidebar( 'hl_intro_box' ) ) dynamic_sidebar( 'hl_intro_box' ) ?>
                </div>
                <div class="col-md-1"></div>
                <div class="clearfix"></div>
            </div>

            <div class="list-product-us">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2><?php echo __( 'SẢN PHẨM CỦA CHÚNG TÔI', 'hoangliet' ) ?></h2>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    $terms = get_terms( [
                        'hide_empty'    => false,
                        'taxonomy'      => 'product_categories'
                    ] );
                    if ( ! empty( $terms ) ) :
                        foreach( $terms as $term ) :
                    ?>
                    <div class="col-md-6">
                        <div class="item">
                            <div class="image">
                                <?php
                                $term_meta = hl_get_product_categories_meta( 'hl_cat_image', $term->term_id );
                                $term_image = wp_get_attachment_image_src( $term_meta, 'full' );
                                ?>
                                <img src="<?php echo $term_image ? $term_image[0] : ''; ?>" alt="<?php echo $term->name ?>">
                            </div>
                            <div class="content">
                                <h4><a href="<?php echo get_term_link( $term->term_id ) ?>" title="<?php echo $term->name ?>"><?php echo $term->name ?></a></h4>
                                <?php echo $term->description ?>
                                <div style="margin-top: 15px"><a title="<?php echo __( 'Xem thêm', 'hoangliet' ) ?>" href="<?php echo get_term_link( $term->term_id ) ?>" class="btn bd-blue"><?php echo __( 'Xem thêm', 'hoangliet' ) ?></a></div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; endif; ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!--End Home Product-->

    <?php get_template_part( 'template-parts/featured-products' ) ?>

    <!--Begin Home News-->
    <div class="home-news">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title"><?php echo __( 'TIN TỨC', 'hoangliet' ) ?></h2>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <?php
                $posts = get_posts( [
                    'posts_per_page'   => 3,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'post',
                    'post_status'      => 'publish',
                    'meta_key'         => 'hlmt_hl_show_in_homepage',
                    'meta_value'       => '1',
                ] );
                if ( ! empty ( $posts ) ) :
                    $first_post = $posts[0];
                    array_shift( $posts );
                ?>
                <div class="col-md-7">
                    <div class="news-hot list-guide">
                        <div class="item">
                            <div class="image">
                                <?php echo get_the_post_thumbnail( $first_post->ID ) ?>
                            </div>
                            <div class="content">
                                <a href="<?php echo get_the_permalink($first_post->ID) ?>" title="<?php echo $first_post->post_title ?>"><h6><?php echo $first_post->post_title ?></h6></a>
                                <p class="date-comment"><i class="fa fa-calendar"></i> <?php echo get_the_date( 'd/m/Y', $first_post->ID ) ?> <span><?php $comment_count = get_comment_count( $first_post->ID ); echo $comment_count['approved']; ?> <?php echo __( 'bình luận', 'hoangliet' ) ?></span></p>
                                <p>
                                    <?php echo get_the_excerpt( $first_post->ID ) ?>
                                </p>
                                <a href="<?php echo get_the_permalink( $first_post->ID ) ?>" class="btn bd-blue"><?php echo __( 'Xem thêm', 'hoangliet' ) ?></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="list-guide">
                        <?php foreach( $posts as $post ) : ?>
                        <div class="item">
                            <div class="image">
                                <?php echo get_the_post_thumbnail( $post->ID ) ?>
                            </div>
                            <div class="content">
                                <a href="<?php echo get_the_permalink($post->ID) ?>" title="<?php echo $first_post->post_title ?>"><h6><?php echo $post->post_title ?></h6></a>
                                <p class="date-comment"><i class="fa fa-calendar"></i> <?php echo get_the_date( 'd/m/Y', $post->ID ) ?> <span><?php $comment_count = get_comment_count( $first_post->ID ); echo $comment_count['approved']; ?> <?php echo __( 'bình luận', 'hoangliet' ) ?></span></p>
                                <p>
                                    <?php echo get_the_excerpt( $post->ID ) ?>
                                </p>
                                <a href="<?php echo get_the_permalink( $post->ID ) ?>" class="btn bd-blue"><?php echo __( 'Xem thêm', 'hoangliet' ) ?></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php endforeach; ?>
                </div>
                <?php endif; ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--End Home News-->

</section>
<!--End Main Body-->
<?php get_footer() ?>