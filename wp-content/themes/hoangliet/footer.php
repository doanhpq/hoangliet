<?php
global $option_tree;
?>
<!--Begin Footer-->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div id="logo">
                        <a href="<?php echo home_url(); ?>">
                            <img src="<?php echo $option_tree['hl_opt_logo'] ?>" alt="logo">
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="info-company">
                        <?php if ( is_active_sidebar( 'hl_footer_1' ) ) dynamic_sidebar( 'hl_footer_1' ) ?>
                    </div>
                    <div class="wrap-social">
                        <p>
                            <a href="<?php echo $option_tree['hl_opt_google'] ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                            <a href="<?php echo $option_tree['hl_opt_linkin'] ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            <a href="<?php echo $option_tree['hl_opt_facebook'] ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="<?php echo $option_tree['hl_opt_twitter'] ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </p>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="menu-footer">
                        <?php wp_nav_menu( [
                            'theme_location'  => 'footer_menu',
                            'container' => ''
                        ] ); ?>
                    </div>
                    <div class="text-search">
                        <div class="row">
                            <div class="col-md-4">
                                <?php if ( is_active_sidebar( 'hl_footer_2' ) ) dynamic_sidebar( 'hl_footer_2' ) ?>
                            </div>
                            <div class="col-md-4">
                                <?php if ( is_active_sidebar( 'hl_footer_3' ) ) dynamic_sidebar( 'hl_footer_3' ) ?>
                            </div>
                            <div class="col-md-4">
                                <?php if ( is_active_sidebar( 'hl_footer_4' ) ) dynamic_sidebar( 'hl_footer_4' ) ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="#" target="_blank"><?php echo $option_tree['hl_opt_footer_left_info'] ?></a>
                    <span class="coppyright"><?php echo $option_tree['hl_opt_footer_right_info'] ?></span>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--End Footer-->
</div>
<!--End Javascript-->
<!--Begin Javascript-->
<?php wp_footer() ?>
<!--End Javascript-->
</body>
</html>